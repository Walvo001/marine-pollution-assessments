# Spatio-temporal aggregation with uncertainty

```{r}
#| include: false

library(tidyverse)
library(litteR)
library(DT)
library(ggrepel)
library(plotly)
library(hues)
library(furrr)
source("utils.R")

# https://github.com/rstudio/DT/issues/533
options(DT.TOJSON_ARGS = list(na = 'string'))

```

```{r}
d_ltr <- read_rds("./data/derived/litter-data.rds") |>
    arrange(country_name, location_code)
```



## Introduction

In this report, we present the results at several temporal and spatial aggregation levels:

1. From litter items to litter groups:
    - TC: total count;
    - SUP: single use plastics;
    - FISH: fisheries-related litter;
    - BAG: plastic bags;
1. Temporal aggregation: aggregate counts per survey for
    - 2015-2016;
    - 2020-2021
1. Spatial aggregation: aggregate counts for
    - individual beaches;
    - countries/subregions (_e.g._, France has two subregions, the North East Atlantic Ocean and the Mediterranean Sea);
    - countries `r d_ltr |> pull("country_code") |> unique() |> sort() |> enumerate()`;
    - subregions: `r d_ltr |> pull("subregion_name") |> unique() |> sort() |> enumerate()`;
    - regions: `r d_ltr |> pull("region_name") |> unique() |> sort() |> enumerate()`;
    - Europe.

We use the [median](https://en.wikipedia.org/wiki/Median) as summary statistic for spatial aggregation. The median is more robust for extreme values than the mean.

To prevent intensively monitored years and/or areas from dominating more sparsely monitored years/areas, we aggregate in a hierarchical way. For example, the median total count (TC) is calculated as:

1. total count per country, location and date (_i.e._, the total count per survey);
1. median total count per country, location and year, _i.e._, the median of the previous bullet over the survey dates;
1. median total count per country, location, _i.e._, the median of the previous bullet over the years;
1. median total count per country, _i.e._, the median of the previous bullet over the locations in each country.
1. etc.


The procedure is schematically depicted in the figure below. Hover over the
points in the figure to see more information.

```{r}
#| warning: false

# sort data to prevent overlapping of the edges of the graph
d <- d_ltr |>
    distinct(country_name, region_name, subregion_name, location_name) |>
    arrange(region_name, subregion_name, country_name, location_name)

# beach level
n <- nrow(d)
dt <- 2 * pi / n
t <- seq(dt, 2 * pi, by = dt) - 0.1 * pi
d$x1 <- 1000 * cos(t)
d$y1 <- 1000 * sin(t)

# subregion-country level
e <- d |>
    distinct(subregion_name, country_name)
n <- nrow(e)
dt <- 2 * pi / n
t <- seq(dt, 2 * pi, by = dt)
e$x2 <- 750 * cos(t)
e$y2 <- 750 * sin(t)
d <- d |>
    left_join(e)

# subregion level
e <- d |>
    distinct(subregion_name)
n <- nrow(e)
dt <- 2 * pi / n
t <- seq(dt, 2 * pi, by = dt)
e$x3 <- 500 * cos(t)
e$y3 <- 500 * sin(t)
d <- d |>
    left_join(e)

# region level
e <- d |>
    distinct(region_name)
n <- nrow(e)
dt <- 2 * pi / n
t <- seq(dt, 2 * pi, by = dt) - 0.4 * pi
e$x4 <- 250 * cos(t)
e$y4 <- 250 * sin(t)

d <- d |>
    left_join(e)

# legend with Ito colors (color blind)
leg <- c(
    beach = rgb(.35, .70, .90),
    `subregion/country` = rgb(0, .60, .50),
    subregion = rgb(.80, .40, 0),
    region = rgb(.90, .60, 0),
    EU = rgb(0, .45, .70))

d$x0 <- 0
d$y0 <- 0

g <- ggplot(data = d) +
    geom_segment(
        mapping = aes(x = x1, y = y1, xend = x2, yend = y2),
        colour = "gray80") +
    geom_segment(
        mapping = aes(x = x2, y = y2, xend = x3, yend = y3),
        colour = "gray80") +
    geom_segment(
        mapping = aes(x = x3, y = y3, xend = x4, yend = y4),
        colour = "gray80") +
    geom_segment(
        mapping = aes(x = x4, y = y4, xend = 0, yend = 0),
        colour = "gray80") +
    geom_point(
        mapping = aes(x = x1, y = y1, colour = "beach", text = location_name),
        size = 0.5) +
    geom_point(
        mapping = aes(x = x2, y = y2, colour = "subregion/country", text = country_name),
        size = 1) +
    geom_point(
        mapping = aes(x = x3, y = y3, colour = "subregion", text = subregion_name),
        size = 2.5) +
    geom_point(
        mapping = aes(x = x4, y = y4, colour = "region", text = region_name),
        size = 5) +
    geom_point(
        mapping = aes(x = x0, y = y0, colour = "EU", text = "EU"),
        size = 10) +
    scale_color_manual(
        name = "spatial scale",
        values = leg) +
    coord_fixed() +
    theme_void()
ggplotly(p = g, width = 900, height = 900, tooltip = "text")
```


```{r}
BASELINE_PERIOD <- 2015:2016
ASSESSMENT_PERIOD <- 2019:2020
```

```{r, child="spatio-temporal-aggregation-nanny-bootstrap.qmd"}
```

```{r}
BASELINE_PERIOD <- 2015:2016
ASSESSMENT_PERIOD <- 2020:2021
```

```{r, child="spatio-temporal-aggregation-nanny-bootstrap.qmd"}
```