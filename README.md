# README

This repository provides [R-code](https://www.r-project.org/) that has been used to analyse the European beach litter data (2015-2021). The R-code is part of a [Quarto](https://quarto.org/)-document. The document layout is given in the `_quarto.yml` file. Note that the beach litter data themselves are _not_ provided in this repository.